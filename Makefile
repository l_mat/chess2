CC = g++
CFLAGS = -Wall -g -std=c++11
LFLAGS = -c -g -std=c++11
EXEC = bin/chess

SOURCES := $(wildcard src/*.cpp)
HEADERS := $(wildcard src/*.h)
OBJ := $(SOURCES:.cpp=.o)
OBJECTS := ${subst src, obj, $(OBJ)}

$(EXEC): $(OBJECTS)
	@echo "sources: " $(SOURCES)
	@echo "objects: " $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(EXEC)

obj/main.o: src/main.cpp
	$(CC) $(LFLAGS) src/main.cpp -o obj/main.o
obj/board.o: src/board.cpp
	$(CC) $(LFLAGS) src/board.cpp -o obj/board.o
obj/figures.o: src/figures.cpp
	$(CC) $(LFLAGS) src/figures.cpp -o obj/figures.o
obj/ui.o: src/ui.cpp
	$(CC) $(LFLAGS) src/ui.cpp -o obj/ui.o
obj/draw.o: src/draw.cpp
	$(CC) $(LFLAGS) src/draw.cpp -o obj/draw.o
obj/figure_map.o: src/figure_map.cpp
	$(CC) $(LFLAGS) src/figure_map.cpp -o obj/figure_map.o
clean:
	rm -f $(EXEC) obj/*.o
