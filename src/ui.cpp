#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include "board.h"
#include "figures.h"

using namespace std;

/*
 *  Read options from configuration file
 *
 *    width 8
 *    height 6
 *    queens 1
 *  ... etc.
 *
 */
bool readOptions(std::unordered_map<std::string, int>& options, string filename) {
string key;
int val;
bool result = true;
   ifstream file(filename);
   std::stringstream e;
  if (file.is_open()) {
    while ( file >> key) {
      if(!options.count(key)) {
        e << "Incorrect option in file: " << key << endl;
        result = false;
        break;
      }
      if(!(file >> val)) {
        e << "No value specified: " << key << endl;
        result = false;
        break;
      }
      options[key] = val;
    }
    file.close();
  } else {
    e << "Unable to open file\n";
    result = false;
  }
  if(!result)
    throw std::invalid_argument(e.str());
  return result;
}

bool parse_position(std::string str,position_t& pos) {
  char ccol;
  size_t row, col;
  if(2 != sscanf(str.c_str(), "%c%d", &ccol,&row))
    return false;
  col = toupper(ccol) - 65;
  row--;
  pos.first = col;
  pos.second = row;
  return true;
}

void printHelp() {
  cout << endl;
  cout << "Available commands:" << endl;
  cout << endl;
  cout << " - figure codes are 'k', 'q', 'b', 'n' or 'r', positions a1, h5 etc.m" << endl;
  cout << endl;
  cout << "add   - add figures to the chessboard" << endl;
  cout << " - add <position> <figure> - add a figure on certain position" << endl;
  cout << " - add <figure_1><figure_2><figure_n> - use the solver to put the figures" << endl;
  cout << endl;
  cout << "rm    - delete figures from the chessboard" << endl;
  cout << " - rm <position> - remove a figure at a certain position" << endl;
  cout << " - rm - remove all figures" << endl;
  cout << endl;
  cout << "write - write the content of the chessboard to file out.txt" << endl;
  cout << endl;
  cout << "test  - run automatic tests" << endl;
  cout << endl;
  cout << "q     - quit" << endl;
  cout << endl;
}

void autoTests() {
#define PAUSE() cout << "Drawing the board:\n"; cout << b << endl; cout << "Type something: "; cin >> ch;
  board b(8,8);
  position_t pos(2,2), pos2(3,4), pos3(12,45);
  figure *n = new knight(pos2);
  char tmp;
  cout << "\AUTOMATIC TESTS:\n\n";
  cout << "Created a test board.\n";
  figure_char ch = 'k';
  cout << "Adding figure "<< ch<<" to " << pos << endl;
  bool result = (b += makeFigure(ch, pos));
  cout << (result ? "Figure added." : "Figure was not added.") << endl;
  cout << "Checking what is on " << pos <<  ": '" << b.figureAt(pos) << "'\n";
  cout << "Checking what is on " << pos2 <<  ": '" << b.figureAt(pos2) << "'\n";
  cout << "Trying to put a figure on the same square.\n";
  
  PAUSE()
  
  result = false;
  bool empty = b.square_empty(pos);
  if(empty)
    result = b += makeFigure('k', pos);
  cout << "Square " << pos << (empty ? " is empty." : " is not empty.") << endl;
  cout << (result ? "Figure added." : "Figure was not added.") << endl;
  cout << "Testing if square " << pos2 << " is empty: " << (b.square_empty(pos2) ? "true" : "false") << endl;
  cout << "Testing if square " << pos2 << " is threatened: " << (b.captured(pos2) ? "true" : "false") << endl;
  cout << "Testing if square " << pos << " would be threatened by new knight at " << pos2 << ": " << (n->captures(pos.first,pos.second) ? "true" : "false") << endl;
  
  cout << "Checking what is on " << pos2 <<  ": '";
  try {
	 cout << b.figureAt(pos2) << "'\n";
  }
  catch(exception& e) {
	  cout << "Exception: " << e.what() << endl;
  }
  cout << "Deleting " << b.figureAt(pos) << " from " << pos << endl;
  b.remove(pos);

  PAUSE()  

  cout << "Checking what is on " << pos <<  ": '" << b.figureAt(pos) << "'\n";
  bool captured = b.captured(pos2);
  empty = b.square_empty(pos2);
  cout << "Checking if could put a knight ";
  if(empty && !captured) {
    cout << "Test ok.\n";
    result = b += makeFigure('n', pos2);
  }
  cout << "Checking what is on " << pos2 <<  ": '" << b.figureAt(pos2) << "'\n";
  bool contains = b.contains(pos3);
  cout << "Checking if board has square " << pos3 <<  ": '" << (contains ? "true" : "false") << "'\n";
  cout << "Checking what is on " << pos3 <<  ": '" << b.figureAt(pos3) << "'\n";
  cout << "Drawing the board:\n";
  cout << b << endl;
  cout << "\<<< END OF AUTOMATIC TESTS.\n\n";
  delete n;

}
