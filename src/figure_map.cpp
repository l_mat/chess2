#include <stdexcept>
#include "figures.h"
#include "figure_map.h"

using key_type = figure_map::key_type;
using iterator = figure_map::iterator;
using value_type = figure_map::value_type;

const figure_map::value_type& figure_map::operator[] (const key_type where) {
	for(size_t i = 0; i < _size; ++i) {
	  if(data[i]->position == where)
		return data[i];
	}
	figure_ptr invalid = nullptr;
	return invalid;
}

void figure_map::pop() {
	--_size;
}

void figure_map::push_back(figure_ptr ptr) {
	data[_size++] = ptr;
}

void figure_map::remove(const key_type where) {
	for(size_t i = 0; i < _size; ++i) {
	  if(data[i]->position == where)
		data[i] = nullptr;
	}
}

bool figure_map::key_exists(const key_type k) {
	for(size_t i = 0; i < _size; ++i)
	  if(data[i]->position == k)
		return true;
	return false;
}

void figure_map::clear() { _size = 0; }

void figure_map::erase(const key_type k) {
	size_t i = 0;
	for(i = 0; i < _size; ++i) {
	  if(data[i]->position == k) {
		data[i] = nullptr;
		break;
	  }
	}
	if(_size > 1 && i < (_size - 1)) {
	  data[i] = data[_size-1];
	}
	--_size;
}

iterator& figure_map::iterator::operator++ () {
  if(index >= _size) throw std::out_of_range("fm_iterator: oor");
  ++index;
  return *this;
}

bool figure_map::iterator::operator== (const iterator& other) {
  return index == other.index;
}

bool figure_map::iterator::operator!= (const iterator& other) {
  return index != other.index;
}

value_type figure_map::iterator::operator* () {
  if(index < _size) {
	return parent->data[index];
  } else throw std::out_of_range("fm_iterator: oor");
}

figure_map::iterator figure_map::begin() {
	return iterator(0, _size, this);
}

figure_map::iterator figure_map::end() {
	return iterator(_size, _size, this);
}
