#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED
#include <ostream>
#include <fstream>
#include <stdexcept>
#include <unordered_map>
#include <map>
#include <vector>
#include <algorithm> // min, max

#include "figures.h"
#include "figure_map.h"

class board {
  static const figure_char EMPTY_SQUARE = ' ';
  size_t **countCaptured;
  using size_type = size_t;
  class iterator;
  class es_iterator; // iterator for operating on empty and not threatened squares only
  size_type width, height;
  //std::map<position_t, figure_ptr> figures;
  figure_map figures;
  board(board &other);
public:
  board(size_type w, size_type h) : width(w), height(h) {
    countCaptured = new size_t* [width];
    for(size_t i = 0; i < width; ++i) {
      countCaptured[i] = new size_t[height];
      for(size_t j = 0; j < height; ++j)
        countCaptured[i][j] = 0;
    }
  }
  ~board() {
    // delete dynamically allocated members
    for(size_t i = 0; i < width; ++i)
      delete [] countCaptured[i];
    delete [] countCaptured;
  }
  bool operator+= (figure_ptr f); // add a figure if neither its location collides nor the figure threatens other
	bool square_empty(position_t pos);
	bool captured(position_t pos);
	figure_char figureAt(position_t pos);
	bool contains(position_t);
	void clear();
	void remove(position_t);
	void move(position_t pos0, position_t pos1);
	bool solve(std::vector<figure_char>::iterator symbols,std::vector<figure_char>::iterator end);
  // get iterators for empty fields
	es_iterator es_begin();
	es_iterator es_end();
	//friend class es_iterator;
	friend std::ofstream& operator<< (std::ofstream& stream,  board& b);
	friend std::ostream& operator<< (std::ostream& stream,  board& b);
};

/*
 *  board::es_iterator - iterator class for accessing empty
 *  and not threatened squares
 */

class board::es_iterator {
  int index, x, y;
  board *b;
public:
  es_iterator(board *bp) : b(bp) {}
  es_iterator& operator++ ();
  position_t operator* ();
  bool operator!= (const es_iterator& other) const;
  friend class board;
};

#endif // BOARD_H_INCLUDED
