#include <ostream>
#include <iomanip>
#include <sstream>
#include "draw.h"
#include "figures.h"
#include <iostream>

/*
 *  print the board on an ansi terminal
 *
 *  with numeric row indexes
 *  and alphabetic column indexes
 */

std::string ansiEscSeq(int code) {
  std::stringstream s;
  static const char escapeChar = 0x1B;
  s << escapeChar << "[" << code << "m";
  return s.str();
}

template <typename T>
std::string printSquare(int x, int y, T c, bool color_flag = 1) {
  std::stringstream stream;
  static const char ESC = 0x1B;
  int textColor = ((x+y)%2) ? 43 : 47;
 // if(color_flag == 0)
  //  stream << ansiEscSeq(37); // set white text color
 // else
   stream << ansiEscSeq(30); // set black text color
  stream << ansiEscSeq(textColor);
  stream << std::setw(2) << c<<" ";
  stream << ansiEscSeq(0); // reset to default format
  return stream.str();
}

template <typename T>
std::string printLabel(T n) {
  std::stringstream stream;
  stream << std::setw(2) <<n<<" ";
  return stream.str();
}

std::ostream& operator<< (std::ostream& stream, board & b) {
  // print column indexes on top
  stream << printLabel<char>(' ');
  for(int j = 0; j < b.width; j++)
    stream << printLabel<char>('A' + j);
  stream << std::endl;
  // print rows
  for(int i = b.height-1; i >= 0; i--) {
    stream << printLabel<int>(i+1);
    for(int j = 0; j < b.width; j++) {
      position_t pos(j,i);
      if(b.square_empty(pos) && b.captured(pos))
        stream << printSquare(j,i,'.');
      else
        stream << printSquare(j,i,b.figureAt(pos),isupper(b.figureAt(pos)));
    }
     stream << printLabel<int>(i+1);
    stream << std::endl;
  }
  // column indices on bottom
  stream << printLabel<char>(' ');
  for(int j = 0; j < b.width; j++)
     stream << printLabel<char>('A'+j);
  stream << std::endl;

  return stream;
}
