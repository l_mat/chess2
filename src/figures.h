#ifndef FIGURES_H_INCLUDED
#define FIGURES_H_INCLUDED
#include <tuple>
#include <ostream>
#include <cstdlib>
#include <memory> // shared_ptr

class figure;

// type for figure position or chessboard square address
// column (letter) first, row (number) second
struct position_t {
  size_t first;
  size_t second;
  position_t() : first(0), second(0) {}
  position_t(const size_t f, const size_t s) : first(f), second(s) {}
  position_t(const position_t& other) : first(other.first), second(other.second) {}
  position_t operator= (const position_t& other) { first = other.first; second = other.second; }
  bool operator<(const position_t& other) const;
  bool operator== (const position_t& other) const;
  friend std::ostream& operator<< (std::ostream& stream, const position_t& fig);
};

using figure_char = char;
using figure_ptr = std::shared_ptr<figure>;

figure_ptr makeFigure(figure_char what, position_t where);

/*
 *  interface class figure - a figure on a chessboard.
 *
 *  contains figure's position as it is meant to be an element of a sparse array
 *  representing layout of certain number of figures of each kind
 *  and calculate which squares are endangered by the figure.
 */
class figure {
private:
  figure(const figure &);
protected:
  position_t position;
  figure_char symbol;
  bool flag; // black or white; 0 (WHITE) - starts at the lower indexes
public:
  figure() : position(0,0), flag(0) {};
  figure(const position_t pos) : position(pos) {};
  const position_t where(); // get the figure's position
  figure_char what() { return flag == 0 ? tolower(symbol) : toupper(symbol); } // or letter-flag pair
  virtual bool captures(size_t x, size_t y) = 0;
  friend std::ostream& operator<< (std::ostream& stream, figure& fig);
  friend class figure_map;
};

/*
 *  each figure class implements functions declared in figure class as pure virtual:
 *  - what() - returns a character representing the kind of figure
 *  - captures(x,y) - tells if the figure (possibly) threatens certain square
 */

class pawn : public figure {
public:
  pawn(const position_t pos) : figure(pos) { symbol = 'P'; };
  bool captures(size_t x, size_t y) {
    // the pawn goes "upwards" (in terms of index numbers) if flag == 0, therefore add 1, else subtract 1
    return (x == position.first - 1 || x == position.first + 1) && y == position.second + (flag == 0 ? 1 : -1 );
  }
};

class rook : public figure {
public:
  rook(const position_t pos) : figure(pos) { symbol = 'R'; };
  bool captures(size_t x, size_t y) {
    return x == position.first || y == position.second;
  }
};

class knight : public figure {
public:
  knight(const position_t pos) : figure(pos) { symbol = 'N'; };
  bool captures(size_t x, size_t y) {
    int dx = position.first - x,
        dy = position.second - y;
    return (abs(dx) == 2 && abs(dy) == 1) || (abs(dx) == 1 && abs(dy) == 2);
  }
};

class bishop : public figure {
public:
  bishop(const position_t pos) : figure(pos) { symbol = 'B'; };
  bool captures(size_t x, size_t y) {
    return (position.first + position.second == x + y) || (position.second - position.first == y - x);
  }
};

class queen : public figure {
public:
  queen(const position_t pos) : figure(pos) { symbol = 'Q'; };
  bool captures(size_t x, size_t y) {
    return (x == position.first || y == position.second) || // row/col
           (position.first + position.second == x + y) || // diag
           (position.second - position.first == y - x);
  }
};

class king : public figure {
public:
  king(const position_t pos) : figure(pos) { symbol = 'K'; };
  bool captures(size_t x, size_t y) {
    return abs(position.first - x) <= 1 && abs(position.second - y) <= 1;
  }
};

#endif // FIGURES_H_INCLUDED
