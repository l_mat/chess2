#include "figures.h"

// figure factory
// returns a smart pointer to a newly created figure object
// if the character was not recognized, returns figure_ptr equal to nullptr
figure_ptr makeFigure(figure_char symbol, position_t position) {
  switch(symbol) {
    case 'p': return std::make_shared<pawn>(position);
    case 'r': return std::make_shared<rook>(position);
    case 'n': return std::make_shared<knight>(position);
    case 'b': return std::make_shared<bishop>(position);
    case 'q': return std::make_shared<queen>(position);
    case 'k': return std::make_shared<king>(position);
    default:
      figure_ptr invalid = nullptr;
      return invalid;
  }
}

bool position_t::operator<(const position_t& other) const {
  return (first + 1000 * second) < (other.first + 1000 * other.second);
}

bool position_t::operator==(const position_t& other) const {
  return first == other.first && second == other.second;
}

const position_t figure::where() {
  return position;
}

std::ostream& operator<< (std::ostream& stream, figure& fig) {
  stream << fig.what();
  return stream;
}

// displaying postion_t in <letter><number> format
std::ostream& operator<< (std::ostream& stream, const position_t& pos) {
  stream << static_cast<char>('A' +pos.first) <<  (pos.second + 1);
  return stream;
}
