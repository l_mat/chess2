#ifndef FIGURE_MAP_H
#define FIGURE_MAP_H

#include <stdexcept>
#include "figures.h"

class figure_map {
public:
  using value_type = figure_ptr;
  using key_type = position_t;
  figure_map() : _size(0) { };
  ~figure_map(){};
  const value_type& operator[] (const key_type where);
  void pop();
  void push_back(figure_ptr ptr);
  void remove(const key_type where);
  bool key_exists(const key_type k);
  void clear();
  void erase(const key_type k);

  class fm_iterator {
  private:
    size_t index, _size;
    figure_map* parent;
  public:
    fm_iterator(size_t i = 0, size_t s = 0, figure_map* p = nullptr) : index(i), _size(s), parent(p) { }
    fm_iterator& operator++ ();
    bool operator== (const fm_iterator& other) ;
    bool operator!= (const fm_iterator& other);
    value_type operator* ();
  };
  using iterator = fm_iterator;

  iterator begin();
  iterator end();
private:
  size_t _size;
  value_type data[1024];
};

#endif // FIGURE_MAP_H
