#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <string>
#include <vector>
#include <stdio.h>
#include <ctime>
#include <cstdlib> // srand

#include "board.h"
#include "figures.h"
#include "ui.h"
#include "draw.h"

using namespace std;

int main()
{
  int width = 8, height = 8;
  const int DIM_MIN = 2, DIM_MAX = 20;

  // to check if symbols are ok
  const string figure_symbols = "kqbnr";

  /* board and solver options */
  std::unordered_map<std::string, int> options;
  options["width"] = 8;
  options["height"] = 8;
  options["rooks"] = 0;
  options["knights"] = 0;
  options["bishops"] = 0;
  options["queens"] = 0;
  options["kings"] = 0;
  
  try {
	  if(!readOptions(options,"./options.txt")) {
		cout << "Incorrect options file format." << endl;
		
	  }
  }
  catch(exception& e) {
	  cout << "Exception: " << e.what() << endl;
	  exit(-1);
  }
  
  bool solver = (options["rooks"] + options["knights"] + options["bishops"] + options["queens"] + options["kings"]) > 0;

  if(auto w = options.find("width") != options.end()) {
    width = min(DIM_MAX,max(DIM_MIN,options["width"]));
  }
  if(auto h = options.find("height") != options.end()) {
    height = min(DIM_MAX,max(DIM_MIN,options["height"]));
  }
  /* create a chessboard object */
  board chessboard(width, height);


  vector<figure_char>toAdd;
  if(solver) {
    cout << "Options for solver:" << std::endl;
    for(auto k : options)
      cout << k.first << "=" << k.second <<  endl;

    for(int i = 0; i < options["queens"]; ++i)
      toAdd.push_back('q');
    for(int i = 0; i < options["bishops"]; ++i)
      toAdd.push_back('b');
    for(int i = 0; i < options["rooks"]; ++i)
      toAdd.push_back('r');
    for(int i = 0; i < options["knights"]; ++i)
      toAdd.push_back('n');
    for(int i = 0; i < options["kings"]; ++i)
      toAdd.push_back('k');
    
    std::srand(unsigned(std::time(0)));
    std::random_shuffle(toAdd.begin(),toAdd.end());

    cout << endl;
    if(chessboard.solve(toAdd.begin(),toAdd.end())) {
      cout << "Solution:"<< endl;
      ofstream out("./out.txt");
      out << chessboard;
      out.close();
    } else
      cout << "There is no solution for given figures." << endl;
  }
  /* interactive testing */

  std::string in;
  do {
    // print the chessboard
    cout << endl << chessboard << endl;

    do {
      cout << "Command (? for help, q - exit, ENTER - draw): ";
      std::getline(std::cin, in);
      std::transform(in.begin(), in.end(), in.begin(), ::tolower);

      std::vector<std::string> command;
      istringstream ss(in);
      string str;
      position_t pos(0,0);
      position_t pos2(0,0);
      char what = 'q';

      while(getline(ss, str, ' ' ))
        command.push_back(str);

      if(command.size() < 1)
        break;

      /* parse position for commands that take it as the first argument */
      if(((command[0] == "rm" || command[0] == "what") && command.size() == 2) ||
        (command[0] == "add" && command.size() == 3) || command[0] == "move") {
        if(!parse_position(command[1],pos)) {
          cout << "Incorrect position format! expected a2 etc."<<endl;
          continue;
        }
        if(!chessboard.contains(pos)) {
          cout << "Position out of range!"<<endl;
          continue;
        }
      }
      if(command[0] == "move") {
        if(command.size() == 3) {
        if(!parse_position(command[2],pos2)) {
          cout << "Incorrect position (2) format! expected a2 etc."<<endl;
          continue;
        }
         if(!chessboard.contains(pos)) {
          cout << "Position out of range!"<<endl;
          continue;
        }
        } else cout << "Incorrect argument number!" << endl;
      }

      /* interpret and execute command */
      // command quit
      if(command[0] == "q" && command.size() == 1)
        break;
      else
      // print help
      if((command[0] == "?" || command[0] == "help") && command.size() == 1) {
        printHelp();
      }
      else
      if(command[0] == "rm") {
        switch(command.size()) {
          case 1: // remove all figures
            chessboard.clear();
            break;
          case 2: // remove a figure from pos
            try {
				chessboard.remove(pos);
			}
			catch(exception& e) {
				cout << "Exception: " << e.what() << "!" << endl;
			}
            break;
          default: cout << "Incorrect parameter number!" << endl;
        }
      } else
      if(command[0] == "add") {
      /* Add a single figure or use solver to put a set of figures
         specified by symbols */
        if(command.size() == 2) {
          vector<figure_char> put = {};
          bool ok = true;

          // make a vector of symbols of figures to add
          for(size_t i = 0; i<command[1].size();i++) {
            figure_char c = command[1].at(i);
            if(figure_symbols.find(c) != string::npos)
              put.push_back(c);
            else {
              ok = false;
              break;
            }
          }
          if(ok) { // try to put the set of figures on the chessboard
            if(!chessboard.solve(put.begin(), put.end()))
              cout << "Solution not found." << endl;
          } else cout <<"Incorrect figure symbols!" << endl;
        } else
          if(command.size() == 3) {
            if(command[2].size() > 1) {
              cout << "Expected only single figure symbol!" << endl;
              continue;
            }
            char what = command[2].at(0);
            /* if(figure_symbols.find(what) == string::npos) {
              cout << "Incorrect figure symbol!" << endl;
            } else */
            figure_ptr fig = makeFigure(what, pos);
            if(fig == false) {
              cout << "Figure creation error!" << endl;
            } else
            if(!(chessboard += fig))
              cout << "Cannot put this figure there." << endl;
         } else cout << "Incorrect parameters!" << endl;
      } else
        if(command[0] == "move") {
          if(command.size() == 3) {
            chessboard.move(pos, pos2);
          } else cout << "Incorrect parameters!" << endl;
      } else
      if(command[0] == "what" && command.size() == 2) {
      /* Tell what is on the specified field */
          if(chessboard.square_empty(pos))
            cout << "Square " << pos << " is empty and "
              << (chessboard.captured(pos) ? "threatened" : "not threatened.") << endl;
          else
            cout << "There is " << chessboard.figureAt(pos) << " on " << pos << "." << endl;
      } else
      if(command[0] == "write" && command.size() == 1) {
      /* write the content of the chessboard to a file */
          ofstream out("./out.txt");
          out << chessboard;
          out.close();
      } else if(command[0] == "test" && command.size() == 1) {
      /* tests */
        autoTests();
      } else cout << "Incorrect command or number of parameters." << endl;
    } while(in != "q");
  } while(in != "q");
}


