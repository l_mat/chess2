#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <stdexcept>

#include "board.h"

bool board::operator+= (figure_ptr f) {
  position_t pos = f->where();
  if(!contains(pos)) {
    throw std::out_of_range("position out of range!");
  }
  if(captured(pos)) {
    // CAPTURED
    return false;
  }
  // check if could capture any figure
  for(auto it : figures) {
    position_t p = it->where();
    if(f->captures(p.first,p.second)) {
      // WOULD CAPTURE
      return false;
    }
  }
  // will not threaten any piece on the board
  //figures[f->where()] = f;
  figures.push_back(f);
  for(size_t i = 0; i < width; ++i)
    for(size_t j = 0; j < height; ++j)
      if(f->captures(i,j))
        countCaptured[i][j]++;
  return true;
}

/* check if the square at pos is empty */
bool board::square_empty(position_t pos) {
  return !(figures.key_exists(pos));
}

/* check if the square at pos is captured by any figure */
bool board::captured(position_t pos) {
  return contains(pos) && countCaptured[pos.first][pos.second] > 0;
}

/*
 *  get the symbol of the figure at the given position
 *  or EMPTY_SQUARE
 */
char board::figureAt(position_t pos) {
 if(figures.key_exists(pos))
   return figures[pos]->what();
  else return EMPTY_SQUARE;
}

bool board::contains(position_t pos) {
  bool result = pos.first >= 0 && pos.second >= 0 && pos.first < width && pos.second < height;
  return result;
}

void board::clear() {
  for(size_t i = 0; i < width; ++i)
    for(size_t j = 0; j < height; ++j)
      countCaptured[i][j] = 0;
  figures.clear();
}

void board::remove(position_t pos) {
  if(figures.key_exists(pos)) {
    figure_ptr f = figures[pos];

    /* decrement entries in captures array */
    for(size_t i = 0; i < width; ++i)
      for(size_t j = 0; j < height; ++j)
        if(f->captures(i,j))
          countCaptured[i][j]--;

    figures.erase(pos);
  } else throw std::out_of_range("Removing from an empty square!");
}

void board::move(position_t pos0, position_t pos1) {
  std::cout << "Moving from " << pos0 << " to " << pos1 << std::endl;
}

board::es_iterator& board::es_iterator::operator++() {
  int end_index = b->width * b->height;
  if(index >= end_index)
    throw std::out_of_range("empty square iterator: out of range!");
  else
  while(++index != end_index) {
    x = index % b->width;
    y = index / b->width;

    position_t pos(x,y);
    if(!b->captured(pos) && b->square_empty(pos))
      break;
  }
  return *this;
}

bool board::es_iterator::operator!= (const board::es_iterator& other) const {
  return other.index != index;
}

board::es_iterator board::es_begin() {
  es_iterator result(this);
  result.index = -1;
  ++result;
  return result;
}

board::es_iterator board::es_end() {
  es_iterator result(this);
  result.index = width * height;
  return result;
}

position_t board::es_iterator::operator* () {
  if(index >= b->width * b->height)
    throw std::out_of_range("empty square iterator: out of range!");
  return position_t(x,y);
}

/*
 *  board::solve(symbols_begin,symbols_end) - if possible, put a set of figures represented
 *  by a vector of symbols
 *
 *  the method is implemented as recursion with returns and uses classes iterator listing
 *  only empty and not threatened squares.
 */

bool board::solve(std::vector<char>::iterator symbols,std::vector<char>::iterator end) {
  if(symbols == end) // all figures have been placed on the chessboard
    return true;

  for(es_iterator f = es_begin(); f != es_end(); ++f) {
    figure_ptr fig = makeFigure(*symbols, *f); // TODO - remove fig
    if((*this) += fig) { // TODO and use makeFigure() here (?)
      if(solve(symbols+1,end))
        return true;
      else
       this->remove(*f);
    }
  }
  return false;
}

/*
 *  output board to a file
 *
 *  .........
 *  ....Q....
 *  ..R...... etc.
 */
std::ofstream& operator<< (std::ofstream& stream, board & b) {
  for(int i = b.height-1; i >= 0; i--) {
    for(int j = 0; j < b.width; j++) {
      position_t pos(j,i);
      if(b.figureAt(pos) == board::EMPTY_SQUARE )
       stream << ".";
      else
        stream << b.figureAt(pos);
    }
    if(i!=0) stream << std::endl;
  }
  return stream;
}
