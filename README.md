# Polecenie 2.1. – Szachy #

Zaprojektować interfejs dla rodziny klas reprezentujących figurę szachową na planszy. Interfejs powinien dostarczać m. in. metodę badającą, czy dane pole jest "bite" prze zfigurę. ZSaimplementować algorytm ustawiający zbiór figur w ten sposób, aby nie było "bić".

Algorytmem ustawiania figur może być np. rekurencja z nawrotami (można również zaproponować inny algorytm). Proszę zwrócić uwagę, aby algorytm wykorzystywał jedynie metody interfejsu figury.

Prosty program testowy powinien pozwalać ustawiać dowolny zbiór figur na planszy _n_`&times;`_m_ (np. _k_ hetmanów, _l_ wież oraz _m_ gońców). Rozmiar planszy (plansza może być prostokątna) oraz liczba figur danego typu są parametrami wejściowymi czytanymi z pliku .txt. Wynikiem powinna być informacja, czy ustawienie jest możliwe dla zadanych parametrów oraz, jeśli tak, jedno z rozwiązań.

# Implementacja #

# Kompilacja #

Kompilacja odbywa się za pomocą załączonego Makefile. Wynikiem jest plik wykonywalny bin/chess.

# Program testujący #

Program wykonywalny odczytuje plik *options.txt* znajdujący się w katalogu roboczym. Oczekiwane są tam parametry wejściowe zapisane w formacie:

><parametr0> <wartość0>
><parametr1> <wartość1> itd.

Wartości wszystkich parametrów powinny być liczbami całkowitymi. Można określić wymiary planszy (*width*, *height*) oraz ilości poszczególnych figur do rozstawienia (*rooks*, *knights*, *bishops*, *queens*, *kings*). Przy uruchomieniu programu podejmowana jest próba znalezienia rozwiązania. Jeśli się powiedzie, pierwsze znalezione rozwiązanie jest zapisywane do pliku out.txt tworzonego w katalogu roboczym.

Po uruchomieniu programu wyświetlane jest ewentualne znalezione rozwiązanie i dostępne jest interakcyjne testowanie funkcji zaimplementowanych klas:

1. dodawanie figury w określonym polu
2. usuwanie figury z określonego pola
3. wyświetlanie listy wolnych pól
4. zapisanie bieżącego układu do pliku